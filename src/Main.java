import java.util.Scanner;

public class Main {

      static double sx, sy, ax, ay, vx, vy = 0;
      static double new_sx, new_sy = 0;
      static float dt = 0f;
      static int iterations = 100;
      public static void main(String[] args) {
            Calculate calculate = new Calculate();
            dt = 1/30f;
            Scanner consoleInput = new Scanner(System.in);
            System.out.print("No of Iterations: ");
            iterations = consoleInput.nextInt();
            consoleInput.nextLine();
            System.out.print("No of Calculation Steps: ");
            dt = consoleInput.nextInt();
            dt = 1/dt;
            consoleInput.nextLine();
            System.out.println(dt);
            try {
                  System.out.println("Beispiel: 0 oder 1.343 etc.");
                  System.out.println("sx eingeben");
                  sx = Double.parseDouble(consoleInput.nextLine());
                  System.out.println("sy eingeben");
                  sy = Double.parseDouble(consoleInput.nextLine());
                  System.out.println("ax eingeben");
                  ax = Double.parseDouble(consoleInput.nextLine());
                  System.out.println("ay eingeben");
                  ay = Double.parseDouble(consoleInput.nextLine());
                  System.out.println("vx eingeben");
                  vx = Double.parseDouble(consoleInput.nextLine());
                  System.out.println("vy eingeben");
                  vy = Double.parseDouble(consoleInput.nextLine());
            } catch (NumberFormatException e) {
                  sx = 2;
                  sy = 4;
                  ax = 1.4;
                  ay = 9.81;
                  vx = 8;
                  vy = 3;
            }
            consoleInput.close();
            for (int i = 1; i < iterations; i++) {

                  sx = calculate.calcCoord(sx, ax, vx, dt);
                  sy = calculate.calcCoord(sy, ay, vy, dt);

                  System.out.println("x: [" + sx + "] y: [" + sy + "]");
            }
      }
}