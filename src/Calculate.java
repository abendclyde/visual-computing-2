public class Calculate {

    public double calcCoord(double s, double a, double v, float dt) {
        double result = 0;

        result = s + v * dt + 0.5 * a * dt * dt;
        v = v + a * dt;
        return result;
    }
}
